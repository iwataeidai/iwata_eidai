<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ユーザー登録</title>
<link href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome-animation/0.2.1/font-awesome-animation.css"
	type="text/css" media="all" />
<link rel="stylesheet" type="text/css"
	href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.3.3/semantic.min.css">
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.3.3/semantic.min.js"></script>
<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
	<div class="main-contents">

		<div class="error">
			<c:if test="${ not empty errorMessages }">
				<div class="errorMessages">
					<ul>
						<c:forEach items="${errorMessages}" var="message">
							<c:out value="${message}" /><br>
						</c:forEach>
					</ul>
				</div>
				<c:remove var="errorMessages" scope="session" />
			</c:if>
		</div>

		<div id="signupHeader">
			<a href="management"><i class="fas fa-arrow-left faa-tada animated-hover"></i></a>
		 </div>

		<form class="ui form" action="signup" method="post">
			<div class="ui text container">

				<div class="field">
					<label for="account">ログインID</label>
						<input name="account" id="account" value="${account}" /> <br />
					<label for="password">パスワード</label>
						<input name="password" type="password" id="password" /> <br />
					<label for="confirmationPassword">確認用パスワード</label>
						<input name="confirmationPassword" type="password" id="password" /><br />
					<label for="name">名前</label>
						<input name="name" id="name"value="${name}" /><br />
				</div>

				<div class="two fields">
					<div class="field">
						<label for="blanch">部署</label>
						<select name="branchId">
							<c:forEach items="${branches}" var="branch">
								<option value="${branch.id}" <c:if test="${branch.id == selectedBranchId}">selected</c:if>>${branch.name}</option>
							</c:forEach>
						</select>
					</div>
					<div class="field">
						<label for="department">役職</label>
						<select name="departmentId">
							<c:forEach items="${departments}" var="department">
								<option value="${department.id}" <c:if test="${department.id == selectedDepartmentId}">selected</c:if>>${department.name}</option>
							</c:forEach>
						</select>
					</div>
				</div>
				<button type="submit" id="signupButton"> <i class="fas fa-user-plus faa-tada animated-hover"></i></button>
			</div>
		</form>
		<div class="copyright" >Copyright(c)Iwata Eidai</div>
	</div>
</body>
</html>