<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>新規投稿</title>
<link href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome-animation/0.2.1/font-awesome-animation.css"
	type="text/css" media="all" />
<link rel="stylesheet" type="text/css"
	href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.3.3/semantic.min.css">
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.3.3/semantic.min.js"></script>
<link rel="stylesheet" type="text/css" href="style.css">
</head>

<body>

	<div class="error">
		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="message">
						<c:out value="${message}" /><br>
					</c:forEach>
				</ul>
			</div>
		</c:if>
	</div>

	<div id="messageHeader">
		<a href="./"  id="homeLik"><i class="fas fa-home faa-tada animated-hover"></i></a>
	</div>

	<form class="ui form" action="message" method="post">
		<div class="field">
			<div class="ui  text container" >
			<label for="title">件名(30文字以下)</label>
			<input name="title" value="${inputTitle}"/>
			<label for="category">カテゴリ(10文字以下)</label>
			<input name="category"  value="${inputCategory}"/>
			<label for="message">投稿内容（1,000文字以下）</label>
			<textarea name="text" cols="100" rows="10"  style="overflow:scroll;">${inputText}</textarea>
			<button id="messageSubmit" type="submit"><i class="fas fa-share faa-tada animated-hover"></i></button>
			</div>
		</div>
	</form>

	<div class="copyright" >Copyright(c)Iwata Eidai</div>
</body>
</html>