package iwata_eidai.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import iwata_eidai.beans.User;

@WebFilter(filterName = "authorityFilter", urlPatterns = {"/management", "/status", "/setting", "/signup"})
public class AuthorityFilter implements Filter{

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {

		HttpServletRequest req = (HttpServletRequest)request;

		User user = (User) req.getSession().getAttribute("loginUser");
		HttpSession session = req.getSession();

		if (user.getBranchId() == 1 && user.getDepartmentId() == 1) {
			chain.doFilter(request, response);
		} else {
			List<String> messages = new ArrayList<String>();
			messages.add("権限がありません。");
			session.setAttribute("errorMessages", messages);
			((HttpServletResponse)response).sendRedirect("./");
		}
	}
	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
	}
	@Override
	public void destroy() {
	}
}
