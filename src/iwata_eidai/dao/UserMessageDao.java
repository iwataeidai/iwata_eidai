package iwata_eidai.dao;

import static iwata_eidai.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import iwata_eidai.beans.UserMessage;
import iwata_eidai.exception.SQLRuntimeException;

public class UserMessageDao {

	public List<UserMessage> getUserMessages(Connection connection, int num, String startDate, String endDate, String category) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();

			sql.append("SELECT * ");
			sql.append("FROM messages ");
			sql.append("INNER JOIN users ");
			sql.append("ON messages.user_id = users.id ");
			sql.append("WHERE messages.created_date BETWEEN ? AND ? ");
			if (category != null) {
				sql.append("AND messages.category LIKE ? ");
			}
			sql.append("ORDER BY messages.created_date DESC limit " + num);

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, startDate);
			ps.setString(2, endDate);
			if (category != null) {
				ps.setString(3, "%" + category + "%");
			}
			ResultSet rs = ps.executeQuery();
			List<UserMessage> ret = toUserMessageList(rs);
			return ret;

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<UserMessage> toUserMessageList(ResultSet rs)
			throws SQLException {

		List<UserMessage> ret = new ArrayList<UserMessage>();
		try {
			while (rs.next()) {
				String account = rs.getString("users.account");
				String name = rs.getString("users.name");
				int id = rs.getInt("messages.id");
				int userId = rs.getInt("messages.user_id");
				String title = rs.getString("messages.title");
				String category = rs.getString("messages.category");
				String text = rs.getString("messages.text");
				Timestamp createdDate = rs.getTimestamp("messages.created_date");

				UserMessage message = new UserMessage();
				message.setAccount(account);
				message.setName(name);
				message.setId(id);
				message.setUserId(userId);
				message.setTitle(title);
				message.setCategory(category);
				message.setText(text);
				message.setCreatedDate(createdDate);

				ret.add(message);
			}
			return ret;
		} finally {
			close(rs);
		}
	}
}
