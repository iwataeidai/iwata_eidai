package iwata_eidai.dao;

import static iwata_eidai.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import iwata_eidai.beans.Branch;
import iwata_eidai.exception.SQLRuntimeException;

public class BranchDao {
	public List<Branch> getBranches(Connection connection) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT * FROM branches");

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();
			List<Branch> ret = toBranchList(rs);
			return ret;

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<Branch> toBranchList(ResultSet rs)
			throws SQLException {

		List<Branch> ret = new ArrayList<Branch>();
		try {
			while (rs.next()) {
				String name = rs.getString("branches.name");
				int id = rs.getInt("branches.id");

				Branch branches = new Branch();
				branches.setName(name);
				branches.setId(id);

				ret.add(branches);
			}
			return ret;
		} finally {
			close(rs);
		}
	}
}
