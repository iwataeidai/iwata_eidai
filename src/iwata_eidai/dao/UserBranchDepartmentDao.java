package iwata_eidai.dao;

import static iwata_eidai.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import iwata_eidai.beans.UserBranchDepartment;
import iwata_eidai.exception.SQLRuntimeException;

public class UserBranchDepartmentDao {

	public List<UserBranchDepartment> getUsers(Connection connection, int num) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT * ");
			sql.append("FROM users ");
			sql.append("INNER JOIN branches ");
			sql.append("ON users.branch_id = branches.id ");
			sql.append("INNER JOIN departments ");
			sql.append("ON users.department_id = departments.id ");
			sql.append("ORDER BY users.created_date ASC limit " + num);

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();
			List<UserBranchDepartment> ret = toUserList(rs);
			return ret;

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<UserBranchDepartment> toUserList(ResultSet rs)
			throws SQLException {

		List<UserBranchDepartment> ret = new ArrayList<UserBranchDepartment>();
		try {
			while (rs.next()) {
				int id = rs.getInt("users.id");
				String account = rs.getString("users.account");
				String name = rs.getString("users.name");
				String status = rs.getString("users.is_stopped");
				String branch = rs.getString("branches.name");
				String department = rs.getString("departments.name");

				UserBranchDepartment user = new UserBranchDepartment();
				user.setId(id);
				user.setAccount(account);
				user.setName(name);
				user.setStatus(status);
				user.setBranch(branch);
				user.setDepartment(department);

				ret.add(user);
			}
			return ret;
		} finally {
			close(rs);
		}
	}

	public UserBranchDepartment getSelectedUser(Connection connection, String selectedUserId) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT * ");
			sql.append("FROM users ");
			sql.append("INNER JOIN branches ");
			sql.append("ON users.branch_id = branches.id ");
			sql.append("INNER JOIN departments ");
			sql.append("ON users.department_id = departments.id ");
			sql.append("AND users.id = ? ");

			ps = connection.prepareStatement(sql.toString());
			ps.setString(1, selectedUserId);

			ResultSet rs = ps.executeQuery();
			UserBranchDepartment ret = toSelectedUser(rs).get(0);
			return ret;

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<UserBranchDepartment> toSelectedUser(ResultSet rs)
			throws SQLException {

		List<UserBranchDepartment> ret = new ArrayList<UserBranchDepartment>();
		try {
			while (rs.next()) {
				int id = rs.getInt("users.id");
				String account = rs.getString("users.account");
				String name = rs.getString("users.name");
				String status = rs.getString("users.is_stopped");
				String branch = rs.getString("branches.name");
				int branchId = rs.getInt("branches.id");
				String department = rs.getString("departments.name");
				int departmentId = rs.getInt("departments.id");

				UserBranchDepartment user = new UserBranchDepartment();
				user.setId(id);
				user.setAccount(account);
				user.setName(name);
				user.setStatus(status);
				user.setBranch(branch);
				user.setBranchId(branchId);
				user.setDepartment(department);
				user.setDepartmentId(departmentId);

				ret.add(user);
			}
			return ret;
		} finally {
			close(rs);
		}
	}
}