package iwata_eidai.dao;

import static iwata_eidai.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import iwata_eidai.beans.UserComment;
import iwata_eidai.exception.SQLRuntimeException;


public class UserCommentDao {

	public List<UserComment> getUserComments(Connection connection, int num) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT * ");
			sql.append("FROM comments ");
			sql.append("INNER JOIN users ");
			sql.append("ON comments.user_id = users.id ");
			sql.append("ORDER BY comments.created_date ASC limit " + num);

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();
			List<UserComment> ret = toUserCommentsList(rs);
			return ret;

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<UserComment> toUserCommentsList(ResultSet rs)
			throws SQLException {

		List<UserComment> ret = new ArrayList<UserComment>();
		try {
			while (rs.next()) {
				int id = rs.getInt("comments.id");
				int userId = rs.getInt("comments.user_id");
				int messageId = rs.getInt("comments.message_id");
				String name = rs.getString("users.name");
				String text = rs.getString("comments.text");
				Timestamp createdDate = rs.getTimestamp("comments.created_date");

				UserComment userComment = new UserComment();
				userComment.setId(id);
				userComment.setUserId(userId);
				userComment.setMessageId(messageId);
				userComment.setName(name);
				userComment.setText(text);
				userComment.setCreatedDate(createdDate);

				ret.add(userComment);
			}
			return ret;
		} finally {
			close(rs);
		}
	}
}
