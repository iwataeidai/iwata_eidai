package iwata_eidai.service;

import static iwata_eidai.utils.CloseableUtil.*;
import static iwata_eidai.utils.DBUtil.*;

import java.sql.Connection;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import iwata_eidai.beans.Message;
import iwata_eidai.beans.UserMessage;
import iwata_eidai.dao.MessageDao;
import iwata_eidai.dao.UserMessageDao;

public class MessageService {

	public void register(Message message) {

		Connection connection = null;
		try {
			connection = getConnection();

			MessageDao messageDao = new MessageDao();
			messageDao.insert(connection, message);

			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	private static final int LIMIT_NUM = 1000;

	public List<UserMessage> getMessage(String selectedStartDate, String selectedEndDate, String Category) {

		Connection connection = null;
		try {
			connection = getConnection();

			Date date = new Date();
			DateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String formattedDate=dateFormat.format(date);
			String defaultStartDate = "2019-01-01 00:00:00";

			String startDate;
			String endDate;

			if (!(StringUtils.isEmpty(selectedStartDate))) {
				startDate = selectedStartDate + " 00:00:00";
			} else {
				startDate = defaultStartDate;
			}

			if (!(StringUtils.isEmpty(selectedEndDate))) {
				endDate = selectedEndDate + " 23:59:59";
			} else {
				endDate = formattedDate;
			}

			UserMessageDao messageDao = new UserMessageDao();
			List<UserMessage> ret = messageDao.getUserMessages(connection, LIMIT_NUM, startDate, endDate, Category);

			commit(connection);

			return ret;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public void delete(int messageId) {

		Connection connection = null;
		try {
			connection = getConnection();

			MessageDao messageDao = new MessageDao();
			messageDao.delete(connection, messageId);

			commit(connection);

		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
}
