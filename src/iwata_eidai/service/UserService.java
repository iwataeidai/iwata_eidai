package iwata_eidai.service;

import static iwata_eidai.utils.CloseableUtil.*;
import static iwata_eidai.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import iwata_eidai.beans.User;
import iwata_eidai.beans.UserBranchDepartment;
import iwata_eidai.dao.UserBranchDepartmentDao;
import iwata_eidai.dao.UserDao;
import iwata_eidai.utils.CipherUtil;

public class UserService {

	public void register(User user) {

		Connection connection = null;
		try {
			connection = getConnection();

			String encPassword = iwata_eidai.utils.CipherUtil.encrypt(user.getPassword());
			user.setPassword(encPassword);

			UserDao userDao = new UserDao();
			userDao.insert(connection, user);

			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public User login(String account, String password) {

		Connection connection = null;
		try {
			connection = getConnection();

			UserDao userDao = new UserDao();
			String encPassword = CipherUtil.encrypt(password);
			User user = userDao.getUser(connection, account, encPassword);

			commit(connection);

			return user;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	private static final int LIMIT_NUM = 1000;

	public List<UserBranchDepartment> getUsers() {

		Connection connection = null;
		try {
			connection = getConnection();

			UserBranchDepartmentDao UserBranchDepartmentDao = new UserBranchDepartmentDao();
			List<UserBranchDepartment> users = UserBranchDepartmentDao.getUsers(connection, LIMIT_NUM);

			commit(connection);

			return users;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public UserBranchDepartment getSelectedUser(String selectedUserId) {

		Connection connection = null;
		try {
			connection = getConnection();

			UserBranchDepartmentDao userBranchDepartmentDao = new UserBranchDepartmentDao();
			UserBranchDepartment user = userBranchDepartmentDao.getSelectedUser(connection, selectedUserId);

			commit(connection);

			return user;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public void update(User user) {

		Connection connection = null;
		try {
			connection = getConnection();
			String newPassword = user.getPassword();

			if (!newPassword.isEmpty()) {
				String encPassword = iwata_eidai.utils.CipherUtil.encrypt(user.getPassword());
				user.setPassword(encPassword);
			}

			UserDao userDao = new UserDao();
			userDao.update(connection, user);

			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public void changeStatus(int id, int status) {

		Connection connection = null;
		try {
			connection = getConnection();

			UserDao userDao = new UserDao();
			userDao.changeStatus(connection, id, status);

			commit(connection);
		} catch(RuntimeException e) {
			rollback(connection);
			throw e;
		} catch(Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public User duplicationCheck(String inputAccount) {

		Connection connection = null;
		try {
			connection = getConnection();

			UserDao userDao = new UserDao();
			User user = userDao.duplicationCheck(connection, inputAccount);

			commit(connection);

			return user;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public User existanceCheck(String selectedUserId) {

		Connection connection = null;
		try {
			connection = getConnection();

			UserDao userDao = new UserDao();
			User user = userDao.existanceCheck(connection, selectedUserId);

			commit(connection);

			return user;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
}
