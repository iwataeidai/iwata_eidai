package iwata_eidai.service;

import static iwata_eidai.utils.CloseableUtil.*;
import static iwata_eidai.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import iwata_eidai.beans.Branch;
import iwata_eidai.dao.BranchDao;

public class BranchService {

	public List<Branch> getBranches() {

		Connection connection = null;
		try {
			connection = getConnection();

			BranchDao branchDao = new BranchDao();
			List<Branch> departments = branchDao.getBranches(connection);

			commit(connection);

			return departments;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
}
