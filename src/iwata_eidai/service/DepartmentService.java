package iwata_eidai.service;

import static iwata_eidai.utils.CloseableUtil.*;
import static iwata_eidai.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import iwata_eidai.beans.Department;
import iwata_eidai.dao.DepartmentDao;

public class DepartmentService {

	public List<Department> getDepartments() {

		Connection connection = null;
		try {
			connection = getConnection();

			DepartmentDao departmentDao = new DepartmentDao();
			List<Department> departments = departmentDao.getDepartments(connection);

			commit(connection);

			return departments;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
}
