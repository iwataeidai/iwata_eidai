package iwata_eidai.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import iwata_eidai.beans.Branch;
import iwata_eidai.beans.Department;
import iwata_eidai.beans.User;
import iwata_eidai.service.BranchService;
import iwata_eidai.service.DepartmentService;
import iwata_eidai.service.UserService;

@WebServlet (urlPatterns = {"/signup"})
public class SignupServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException{

		List<Branch> branches = new BranchService().getBranches();
		request.setAttribute("branches", branches);

		List<Department> departments = new DepartmentService().getDepartments();
		request.setAttribute("departments", departments);

		request.getRequestDispatcher("signup.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		List<String> messages = new ArrayList<String>();
		User user = new User();

		user.setAccount(request.getParameter ("account"));
		user.setPassword(request.getParameter ("password"));
		user.setName(request.getParameter ("name"));
		user.setBranchId(Integer.parseInt(request.getParameter ("branchId")));
		user.setDepartmentId(Integer.parseInt(request.getParameter ("departmentId")));

		String confirmationPassword = request.getParameter ("confirmationPassword");

		User checkingUser = new UserService().duplicationCheck(user.getAccount());

		if (isValid(request, messages, user, confirmationPassword, checkingUser)) {
			new UserService().register(user);
			response.sendRedirect("management");
		} else {

			List<Branch> branches = new BranchService().getBranches();
			List<Department> departments = new DepartmentService().getDepartments();

			request.setAttribute("account", user.getAccount());
			request.setAttribute("name", user.getName());
			request.setAttribute("selectedBranchId", user.getBranchId());
			request.setAttribute("selectedDepartmentId", user.getDepartmentId());

			request.setAttribute("branches", branches);
			request.setAttribute("departments", departments);

			request.setAttribute("errorMessages", messages);
			request.getRequestDispatcher("signup.jsp").forward(request, response);
		}
	}

	private boolean isValid(HttpServletRequest request, List<String> messages,
			User user, String confirmationPassword, User checkingUser) {

		if (StringUtils.isEmpty(user.getAccount())) {
			messages.add("アカウントを入力してください");
		} else if (!(user.getAccount().matches("^[0-9a-zA-Z]{6,20}$"))) {
			messages.add("アカウントは半角英数6~20文字で入力してください");
		}

		if (!(checkingUser == null)) {
			messages.add("アカウントが重複しています");
		}

		if (StringUtils.isEmpty(user.getPassword())) {
			messages.add("パスワードを入力してください");
		} else if  (!(user.getPassword().matches("^[!-~]{6,20}$"))){
			messages.add("パスワードは記号を含む半角文字6～20文字で入力してください");
		}else if (!(confirmationPassword.equals(user.getPassword()))) {
			messages.add("パスワードが一致しません");
		}

		if (StringUtils.isEmpty(user.getName())) {
			messages.add("名前を入力してください");
		} else if  (10 <= user.getName().length()){
			messages.add("名前は10文字以下で入力してください");
		}

		if (user.getBranchId() == 1 && 2 < user.getDepartmentId()) {
			messages.add("選択できない支店または役職です");
		} else if  (2 <= user.getBranchId() && user.getDepartmentId() < 3){
			messages.add("選択できない支店または役職です");
		}

		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}
}
