package iwata_eidai.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import iwata_eidai.beans.User;
import iwata_eidai.service.UserService;

@WebServlet (urlPatterns = { "/login" })
public class LoginServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		request.getRequestDispatcher("login.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		String account = request.getParameter("account");
		String password = request.getParameter("password");
		UserService userService = new UserService();
		User user = userService.login(account, password);

		HttpSession session = request.getSession();

		List<String> messages = new ArrayList<String>();

		if (isValid(request, messages)) {
			session.setAttribute("loginUser", user);
			response.sendRedirect("./");
		} else {
			request.setAttribute("inputAccount", account);
			request.setAttribute("errorMessages", messages);
			request.getRequestDispatcher("login.jsp").forward(request, response);
		}
	}

	private boolean isValid(HttpServletRequest request, List<String> messages) {

		String account = request.getParameter("account");
		String password = request.getParameter("password");
		UserService userService = new UserService();
		User user = userService.login(account, password);

		if (StringUtils.isEmpty(account)) {
			messages.add("ログインIDまたはパスワードが入力されていません");
		} else if (StringUtils.isEmpty(password)){
			messages.add("ログインIDまたはパスワードが入力されていません");
		} else if (user == null) {
			messages.add("ログインIDまたはパスワードが誤っています");
			request.setAttribute("errorMessages", messages);
		} else if (user.getStatus() != 0) {
			messages.add("ログインIDまたはパスワードが誤っています");
			request.setAttribute("errorMessages", messages);
		}

		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}
}