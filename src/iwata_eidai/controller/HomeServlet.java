package iwata_eidai.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import iwata_eidai.beans.UserComment;
import iwata_eidai.beans.UserMessage;
import iwata_eidai.service.CommentService;
import iwata_eidai.service.MessageService;


@WebServlet (urlPatterns = { "/index.jsp" })
public class HomeServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		String selectedStartDate = request.getParameter("selectedStartDate");
		String selectedEndDate = request.getParameter("selectedEndDate");
		String selectedCategory = request.getParameter("selectedCategory");

		List<UserMessage> messages = new MessageService().getMessage
				(selectedStartDate, selectedEndDate, selectedCategory);
		List<UserComment> userComments = new CommentService().getComment();

		request.setAttribute("selectedStartDate", selectedStartDate);
		request.setAttribute("selectedEndDate", selectedEndDate);
		request.setAttribute("selectedCategory", selectedCategory);

		request.setAttribute("messages", messages);
		request.setAttribute("comments", userComments);

		request.getRequestDispatcher("home.jsp").forward(request, response);
	}
}