package iwata_eidai.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import iwata_eidai.beans.Message;
import iwata_eidai.beans.User;
import iwata_eidai.service.MessageService;

@WebServlet(urlPatterns = { "/message" })
public class MessageServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException{
		request.getRequestDispatcher("message.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		HttpSession session = request.getSession();

		List<String> messages = new ArrayList<String>();
		User user = (User) session.getAttribute("loginUser");

		Message message = new Message();
		message.setTitle(request.getParameter("title"));
		message.setCategory(request.getParameter("category"));
		message.setText(request.getParameter("text"));
		message.setUserId(user.getId());

		if (isValid(request, messages, message)) {
			new MessageService().register(message);
			response.sendRedirect("./");
		} else {
			request.setAttribute("inputTitle", message.getTitle());
			request.setAttribute("inputCategory", message.getCategory());
			request.setAttribute("inputText", message.getText());
			request.setAttribute("errorMessages", messages);
			request.getRequestDispatcher("message.jsp").forward(request, response);
		}
	}

	private boolean isValid(HttpServletRequest request, List<String> messages, Message message) {

		if (StringUtils.isBlank(message.getTitle())) {
			messages.add("タイトルを入力してください");
		} else if (30 < message.getTitle().length()) {
			messages.add("タイトルは30文字以下で入力してください");
		}

		if (StringUtils.isBlank(message.getCategory())) {
			messages.add("カテゴリーを入力してください");
		} else if (10 < message.getCategory().length()){
			messages.add("カテゴリーは10文字以下で入力してください");
		}

		if (StringUtils.isBlank(message.getText())) {
			messages.add("本文を入力してください");
		} else if (1000 < message.getText().length()) {
			messages.add("本文は1000文字以下で入力してください");
		}

		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}
}
