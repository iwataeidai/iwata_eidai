package iwata_eidai.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import iwata_eidai.beans.UserBranchDepartment;
import iwata_eidai.service.UserService;

@WebServlet(urlPatterns = { "/management" })
public class ManagementServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException{

		List<UserBranchDepartment> users = new UserService().getUsers();
		request.setAttribute("users", users);

		request.getRequestDispatcher("management.jsp").forward(request, response);
	}
}
