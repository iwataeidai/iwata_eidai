package iwata_eidai.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import iwata_eidai.beans.Comment;
import iwata_eidai.beans.User;
import iwata_eidai.service.CommentService;

@WebServlet(urlPatterns = { "/comment" })
public class CommentServlet extends HttpServlet {

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		HttpSession session = request.getSession();

		List<String> comments = new ArrayList<String>();

		User user = (User) session.getAttribute("loginUser");

		Comment comment = new Comment();
		comment.setText(request.getParameter("comment"));
		comment.setUserId(user.getId());
		comment.setMessageId( Integer.parseInt(request.getParameter("messageId")));

		if (isValid(request, comments, comment)) {
			new CommentService().register(comment);
			response.sendRedirect("./");
		} else {
			session.setAttribute("inputText", comment.getText());
			session.setAttribute("inputMessageId", comment.getMessageId());
			session.setAttribute("errorMessages", comments);
			response.sendRedirect("./");
		}
	}

	private boolean isValid(HttpServletRequest request, List<String> messages, Comment comment) {

		if (StringUtils.isBlank(comment.getText())) {
			messages.add("コメントを入力してください");
		} else if (500 < comment.getText().length()) {
			messages.add("コメントは500文字以内で入力してください");
		}

		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}
}
