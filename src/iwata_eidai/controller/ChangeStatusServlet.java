package iwata_eidai.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import iwata_eidai.service.UserService;


@WebServlet (urlPatterns = { "/status" })
public class ChangeStatusServlet extends HttpServlet {

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		new UserService().changeStatus(Integer.parseInt(request.getParameter("selectedUserId")), Integer.parseInt(request.getParameter("status")));

		response.sendRedirect("management");
	}
}
